---
title: The Craft and Crafting
weight: 135
---

In Pentola there is an approach to magic that seeks to uncover the rules which govern the universe.
The application of these rules to manipulate reality is known as the _Craft_, and those who do so are known as **crafters**.

Practitioners of the Craft develop in one of two ways:

- The majority belong (at least initially) to academic institutions, which have their own books of dweomers and rules that they teach their apprentices.
- There is a long tradition of crafters working in solitude, cut off from other crafters and society at large, to focus purely on their magical activities.
  Occasionally they take on an apprentice to teach their art or use as a helping hand around the magical laboratory.

## Ranks

There are three general ranks of crafters in Pentola, though specific organizations may have different ranks or requirements:

- **Apprentice:** Students of the Craft who will only know a couple of dweomers, usually including Mystic Vision, whose Crafting skill bonus is at least 25%.

  As well as being taught the Craft, they are expected to spend half of their time working for their tutors by performing menial tasks in their magical laboratories or other jobs that their masters consider beneath them.
- **Journeyman:** Graduates of an academic institution.
  They tend to know between five and ten dweomers, and have a Crafting skill bonus ranging from 50% to 90%.
  If a member of an academic organization, they will be expected to spend a third of their time performing duties for the school, such as teaching apprentices or recovering lost magical knowledge.
- **Master:** Acknowledged masters of the Craft.
They know at least ten dweomers and a have a Crafting skill bonus of 90% or higher.
  If a member of an academic organization then they will serve on its ruling body and have the complete resources of the organization at their command.
  In return it is expected that they spend 90% of their time researching, teaching, and performing missions on the school's behalf.

## Learning the Craft

Before a dweomer can be applied using Crafting, it must first be learned through research.
In order to learn a particular dweomer, the character must possess the dweomer in written form, be taught it by a teacher, or discover it via experimentation.

Once the The character then spends two Improvement Points and writes the dweomer down on their character sheet.

Each dweomer is governed by the Crafting skill.
This skill may be improved normally though the use of Improvement Points and practice.
Even non-Crafters have this skill, at a base bonus of 5%, since magic knowledge is widespread and simple spells are often used by normal folk - and nearly everyone in the civilized world interacts with magic items on occasion.

Once the dweomer has been learned, the character will be ready to try casting it.

## Manipulation and Complexity

Dweomers have three basic effects which can be manipulated by the Crafter: magnitude, duration, and range.
Doing so, however, increases the complexity of the dweomer, a measure of how hard it is to work out and apply the dweomer.

Each effect has a default value which the dweomer can be applied at, starting at Complexity 1.
The default value for the dweomer effects are listed in the Manipulation table below.

The tens value of the character's Crafting skill bonus determines the maximum complexity that they can handle on each of the manipulation types.

However, just because a crafter _can_ apply complex dweomers doesn't mean _it is safe to do so_.
The total complexity of a dweomer is the amount of damage that will be inflicted if they fumble its application.

A crafter can divert some of this damage by permanently reducing their power point total by one for each hit point they choose not to lose.
The power points can be sourced from an item, but only one whose power point store is also permanent.

{{% example "Dweomer Complexity" %}}
Alessia has an INT of 10, a crafting skill bonus of 65%, meaning she can handle a complexity of up to 6 on each of the dweomer's effects across magnitude, duration, and range.
That's up to a total complexity score of 24, with no single effect having a rating higher than 6.

Alessia could choose to apply a dweomer with a magnitude of 4 (4 complexity), a Duration of 4 hours (6 complexity), and a range of one furlong (4 complexity) for a total of 14 complexity.

If she fumbles, she'll take 14 damage which can only be mitigated by permanently burning one power point per hit point spared.
{{% /example %}}

| Complexity  | Magnitude |  Duration  |    Range    |        Targets       |
|:-----------:|:---------:|:----------:|:-----------:|:--------------------:|
| 1 (default) |     1     | 1 Minute   | Close       |           1          |
|     +1      |     2     | 5 Minutes  | Nearby      |           2          |
|     +2      |     3     | 15 Minutes | Distant     |           3          |
|     +3      |     4     | 30 Minutes | 100 Yards   |           4          |
|     +4      |     5     | 1 Hour     | 1 Furlong   |  5 or 5 yard radius  |
|     +5      |     6     | 2 Hours    | 2 Furlongs  |           6          |
|     +6      |     7     | 4 Hours    | 5 Furlongs  |           7          |
|     +7      |     8     | 12 Hours   | 1 Mile      |           8          |
|     +8      |     9     | 1 Day      | 2 Miles     |           9          |
|     +9      |     10    | 2 Days     | 5 Miles     | 10 or 10 yard radius |
|     +10     |     11    | 4 Days     | 10 Miles    |           11         |
|     +11     |     12    | 1 Week     | 20 Miles    |           12         |
|     +12     |     13    | 2 Weeks    | 50 Miles    |           13         |
|     +13     |     14    | 1 Month    | 100 Miles   |           14         |
|     +14     |     15    | 2 Months   | 200 Miles   | 15 or 15 yard radius |
|     +15     |     16    | 1 Season   | 500 Miles   |           16         |
|     +16     |     17    | 2 Seasons  | 1000 Miles  |           17         |
|     +17     |     18    | 1 Year     | 2000 Miles  |           18         |
|     +18     |     19    | 2 Years    | 5000 Miles  |           19         |
|     +19     |     20    | 5 Years    | 10000 Miles | 20 or 20 yard radius |

## Applying Dweomers

A character must be able to gesture with their hands and be able to chant in order to apply a dweomer.
Whenever a dweomer is applied there will always be a sight and sound that nearby creatures can detect -  be it a flash of light, a crack of thunder, or a shimmering in the air.
The exact effects are up to the Games Master and Player to decide, but will automatically be detected by any creatures within ten times the Magnitude of the spell in yards.

Dweomers can be applied in two ways:

1. slowly, carefully, in a ritual, or
2. off-the-cuff, making all the requisite calculations mentally.

In the first case, the character must make a Crafting test, taking ten minutes per point of complexity for the dweomer.
Multiple Crafters working together can reduce the time to apply the dweomer, but any one crafter failing their test will cause the dweomer to fail.
The complexity limit of the dweomer is dependent on the Crafter involved with the Dweomer with the highest Crafting skill.
Failure to apply the dweomer means that the effort was wasted and can be retried.
Applying a dweomer slowly and carefully while following safety protocols prevents terrible side effects, including the damage caused by failing or fumbling.

In the second case, the character must make a _hard_ Crafting test to apply the dweomer by making quick mental calculations and adjustments.
Failing the test indicates that the character was unable to correctly adjudicate the energy and manipulations correctly and has caused a _misapplication_ - see the table below.
Fumbling the test causes the crafter to take damage equal to the complexity of the dweomer.
This damage cannot be reduced by AP.

| 1D8 | Misapplication Effect |
|:---:|:----------------------|
|  1  | **Fizzle**: The dweomer's energy dissipates harmlessly around the crafter
|  2  | **Burn:** The dweomer burns through the crafter, the magical energy misdirect. Take 1D4 damage.
|  3  | **Explosion:** The energy of the dweomer goes off catastrophically, causing an explosion whose diameter is equal to the range of the dweomer. The explosion inflicts damage equal to half the total complexity to everyone in the sphere.
|  4  | **Blind:** the dweomer's energy radiates as a brilliant light centered on the crafter, blinding them and anyone who looks at them in the next moment.
|  5  | **Overpower:** The dweomer is applied but with more power than intended; double the magnitude of the dweomer but inflict the additional complexity cost as damage on the crafter.
|  6  | **Memory Loss:** Applying the dweomer without the appropriate safeguards caused the magic to burn the crafter's mind, making them lose some of what they've learned about the craft, though it does still take effect. Reduce the character's crafting skill bonus by a number of points equal to the magnitude of the dweomer.
|  7  | **Weak:** The dweomer is successfully applied, but without any of the intended manipulations.
|  8  | **Delayed:** The crafter fails to apply the dweomer in this moment but may still apply it in the next one automatically and without an additional test.