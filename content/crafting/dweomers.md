---
title: Dweomers
---

The traits used by dweomers are detailed below.

- **Concentration:** The dweomer's effects will remain in place as long as the character concentrates on it, instead of the duration being determined via manipulation.
  Concentrating on a dweomer is functionally identical to applying the dweomer, requiring the Crafter to continue to gesture with both arms, chant and ignore distractions.
- **Instant:** The dweomer's effects take place instantly, instead of the duration being determined via manipulation.
  The dweomer itself then disappears.
- **Permanent:** The dweomer's effects remain in place until they are dispelled or dismissed, instead of the duration being determined via manipulation.
- **Resist:** The dweomer's effects do not take effect automatically.
  The target may make an opposed POW test in order to avoid the effect of the dweomer entirely.
- **Dodge:** The dweomer's effects do not take effect automatically.
  The target may make an opposed test (their BOD vs the crafter's POW) in order to avoid the effect of the dweomer entirely.
  Note that the target must be able to use their reaction in order to dodge.
  In the case of area dweomers, the Dodge trait requires the target to dive in order to mitigate the dweomer's effect.
- **Touch:** Touch dweomers require the character to actually touch his target for the dweomer to take effect, instead of the range being determined via manipulation.
  The crafter must remain in physical contact with the target for the entire application of the dweomer.

  {{% dweomers %}}