---
title: "Calculating Attributes"
weight: 105
---

Once you have rolled for your starting characteristics you can determine your attributes.
These are pools of points which represent your ability to sustain damage (in the case of hit points) and use magic or special abilities (in the case of power points).

+ **Hit Points (HP):** Add your character's BOD and POW, then divide by two (round up) to determine HP.
+ **Power Points (PP):** Add your character's INT and POW, then divide by two (round up) to determine PP.

{{% example "Determining Attributes for Karacter" %}}
Having rolled the characteristics for Karacter we can calculate their attributes:

+ Hit Points: With a BOD of 13 and a POW of 6, Karacter's HP is 9.
+ Power Points: With an INT of 10 and a POW of 6, Karacter's PP is 8.

{{% /example %}}