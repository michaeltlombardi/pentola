---
title: Setting Ambitions
weight: 170
---

At the start of play, give your character two long term ambitions and one short term ambition.

Long term ambitions are things that are life ambitions, only achievable over the period of a linked set of adventures, commonly known as a ‘campaign'.
Short term ambitions are usually relevant to the adventure currently being played, and are determined near the beginning of the session by the players.

Achieving ambitions help you earn improvement points, which you can spend to [improve your character](#improving-characters):

+ Every time an ambition is brought into play in a concrete way, the character earns one improvement per session.
+ Short term ambitions are removed at the end of the session, and if completed earn an additional two improvement points.
+ When a long term ambition is finally achieved it is removed from the player's character sheet and the character earns five improvement points.

{{% example "Setting Ambitions for Karacter" %}}
For Karacter we'll set the following ambitions:

+ Short: Pass the trials required to join the local chapter of the Wardens, a guild of mercenaries specializing in guard duty.
+ Long: Become a Chapter Master of the Wardens.
+ Long: Become the heroic subject of an epic poem.

{{% /example %}}
