---
title: "Relationships"
weight: 120
---

When you begin play, choose an ally, a dependent, and either an enemy or an organization.
You have a relationship bonus with each of these equal to 25%.

+ **Ally**. This is a group or person a player character can ask for aid, can call in favors from, or with whom they otherwise have a positive relationship.
+ **Dependent**. This is usually the character's family or a group that relies upon them.
+ **Enemy**. This is a sworn enemy.
  Interactions with this person or group are usually negative.
  They may be a recurring foe or antagonistic organization.
+ **Organization**.This could be a wizards' sorority, a former regiment or even a cult.
  The character has an association with this organization; they typically deal with this group on a favorable basis.
  GMs should determine the maximum scale of an organization, which could be as great as a whole nation, or just a small wandering band of minstrels.

On a critical success for a relationship test, the character gains a very positive response, and an increase of 1D6% to their relationship.
On a fumble the character damages their relationship with the party and loses 1D4% from their relationship.
Further, after a fumble, the player cannot call on a relationship for rest of the adventure.
In the case of enemies this means that they have accidentally aided them in some way.

| Relationship | IP Cost | Advantages (must make a successful relationship roll) | Disadvantages |
|:------------:|:-------:|:------------------------------------------------------|:--------------|
| Ally         | 2       | Loan an appropriate item or finances; assist on social test; provide info and/or employment | Allies demand favors of the character, in the form of money, errands etc for each favor given; enemies of your ally become your enemies.
| Dependent    | 1       | Characters gain 1-3 additional Improvement Points where they support, aid or defend Dependents. | Dependents regularly get themselves and the character into trouble; Enemies will target dependents to harm the character
| Enemy        | 2       | Gains +25% to tests in the attempt to thwart their enemy;  gain 1-3 additional IP for thwarting an enemy's plans; gain employment by enemy's opponents | Enemies will try to harm characters as often as possible; All interactions (parleys, influence, negotiation) with an enemy will be at one difficulty rating harder than normal.
| Organization | 2       | Can access the facilities offered by the organization; can gain information and possible employment | Organizations typically demand a charge for their services, either in cash or a favor; Enemies of an organization may harry and harm the character for their relationship to the organization.

You can purchase new relationships later with improvement points for the cost listed; new relationships start at a bonus of 10%.
New relationships may arise from the fiction - in these cases, they start at 5%.

{{% example "Relationships in Play" %}}
At the start of play, Karacter selects Warden, their aunt, who as an ally.
Karacter takes Spot, a miniature tiger, as their dependent.
Finally, as Karacter's goal is to become a full fledged member of the Wardens, Karacter takes them as an organizational relationship.

During Karacter's first session it becomes clear that lacking a proper weapon (Karacter only has a single dagger which doubles as their utility knife) is a problem.
Karacter asks their aunt, Warden, for a loaner weapon and makes a relationship test.
Karacter's POW is 11, making the base goal 33% - Karacter adds their relationship bonus, 25%, for a total goal of 58%.

Karacter rolls a 32 and the referee declares that Aunt Warden lends an old shortsword and a spare longbow to Karacter - on the condition that Karacter must succeed in the trials to join the Wardens or return the weapons upon failure.

Later in the adventure, Karacter is ambushed in an alley by Spy, who has been paid by the father of a man Warden killed in a judicial duel - Karacter has inherited their aunt's enemies.
{{% /example %}}

## Allies

| 1D20 | Ally               |
|:----:|:------------------:|
|  01  | Gondoliera         |
|  02  | Butcher            |
|  03  | Spireglider        |
|  04  | Trebuchet Operator |
|  05  | Mercenary          |
|  06  | Innkeeper          |
|  07  | Fisher             |
|  08  | Priest             |
|  09  | Author             |
|  10  | Artist             |
|  11  | Archivist          |
|  12  | Smith              |
|  13  | Alchemist          |
|  14  | Canal Cleanser     |
|  15  | Merchant           |
|  16  | Reporter           |
|  17  | Gladiator          |
|  18  | Notary             |
|  19  | Barrister          |
|  20  | Barber-Surgeon     |

## Dependents

| 1D12 | Dependent       |
|:----:|:---------------:|
|  01  | Foundling Child |
|  02  | Parent          |
|  03  | Aunt/Uncle      |
|  04  | Nephew/Niece    |
|  05  | Cousin          |
|  06  | Friend          |
|  07  | Alchemy Addict  |
|  08  | Client          |
|  09  | Grandparent     |
|  10  | Falcon          |
|  11  | Apprentice      |
|  12  | Drake Hatchling |

## Enemy

| 1D20 | Enemy                                    |
|:----:|:----------------------------------------:|
|  01  | Organization (Roll 1D12+1 on this table) |
|  02  | Mercenary                                |
|  03  | Barber-Surgeon                           |
|  04  | Investigator                             |
|  05  | Fixer                                    |
|  06  | Alchemist                                |
|  07  | Smith                                    |
|  08  | Barrister                                |
|  09  | Knight                                   |
|  10  | Priest                                   |
|  11  | Crafter                                  |
|  12  | Librarian                                |
|  13  | Judicial Duelist                         |
|  14  | Banker                                   |
|  15  | Guild Leader                             |
|  16  | Patron/Matron                            |
|  17  | Automaton                                |
|  18  | Spire Aristocrat                         |
|  19  | Ship Captain                             |
|  20  | Demon                                    |

## Organization

Roll 1D4 to determine the type of organization, then roll on the related table to determine details about the organization.
Use those details as the skeleton of ideas to flesh out a group of people in the world - Pentola is enormous and varied, far too large and fractured to detail every organization who you might encounter.

| 1D4 | Table to Roll On |
|:----:|:----------------:|
|  01  | Academic         |
|  02  | Religious        |
|  03  | Guild            |
|  04  | Spire            |

### Academic Organizations

Roll 1D12 6 times:

- **Type:** The institution of the organization that defines its structure
- **Focus:** The purpose of the organization, what it primarily spends its effort on
- **Symbol:** The marking used to differentiate this organization from others
- **Color:** The primary color associated with the organization, often used in their clothes or banners
- **Reputation:** The strongest belief outsiders have about the organization's behaviors and beliefs
- **Distinctive Feature:** In Pentola the beliefs and repeated behaviors of people change their bodies - members of organizations tend to acquire one or more distinctive features over time.

| 1D12 | Type            | Focus       | Symbol  | Color    | Reputation | Distinctive Feature      |
|:----:|:---------------:|:-----------:|:-------:|:--------:|:----------:|:------------------------:|
|  01  | School          | Automata    | Drake   | Red      | Bold       | Entirely hairless        |
|  02  | Gymnasium       | Crafting    | Book    | Blue     | Pedantic   | Eyes look like coins     |
|  03  | University      | Archaeology | Fist    | Yellow   | Reckless   | Metallic fingernails     |
|  04  | Academy         | Alchemy     | Scroll  | Green    | Bookish    | Bifurcated tongue        |
|  05  | Library         | Politics    | Flower  | Purple   | Greedy     | Hair is same as Color    |
|  06  | Laboratory      | History     | Chains  | Brown    | Proud      | Skin is hard like stone  |
|  07  | Archives        | Literature  | Falcon  | White    | Combative  | Blind in left eye        |
|  08  | Studio          | Arts        | Spire   | Gray     | Rich       | Lightning scars all over |
|  09  | Institute       | Warfare     | Sword   | Black    | Pious      | Tally mark scars on body |
|  10  | Monastery       | Demonology  | Lantern | Orange   | Loyal      | Extra finger on one hand |
|  11  | Gurukula        | Medicine    | Heart   | Pink     | Strange    | Eyes same as Color       |
|  12  | Spire College   | Rhetoric    | Eye     | Numerous | Implacable | Voice always echoes      |

{{% example "Academic Organization" %}}
We first roll 1D12 6 times to determine which details will define this organization:

- **Type:** 9, Institute
- **Focus:** 6, History
- **Symbol:** 1, Drake
- **Color:** 10, Orange
- **Reputation:** 9, Pious
- **Distinctive Feature:** 12, Voice always echoes

### Guardians of the True Word

A snarling bronze drake is embossed upon the doors to this old and prestigious research guild.
The academics within practice psychohistorical magics and divination to discern the Truth of the World as it has been - they then share this interpretation with each other orally, crafting poems as records in a tongue taught only to full members of the guild.
They are funded by religious believers of the True Word but do not evangelize.
Artists of the Word stay with the Guardians for weeks at a time, learning the new discoveries and committing them to memory, that they may go out into the world and create works which echo this truth.

The Truth is never spoken aloud to outsiders, but no word spoken by a believer is ever heard only once - every syllable seems to echo in the air, no matter how hushed the tone.
{{% /example %}}

### Religious Organizations

Roll 1D12 6 times:

- **Type:** The institution of the organization that defines its structure
- **Primary Domain:** The main focus of the religious group in this institution - they may have others, but this is what they're _most_ known for.
- **Symbol:** The marking used to differentiate this organization from others
- **Preferred Sacrifice:** The sacrifice that is considered most valuable to this organization
- **Cleric Attire:** Ranking members of religious organizations are often visible by a special item of clothing
- **Distinctive Feature:** In Pentola the beliefs and repeated behaviors of people change their bodies - members of organizations tend to acquire one or more distinctive features over time.

| 1D12 | Type        | Primary Domain | Symbol   | Preferred Sacrifice | Cleric Attire | Distinctive Feature      |
|:----:|:-----------:|:--------------:|:--------:|:-------------------:|:-------------:|:------------------------:|
|  01  | Church      | Birth          | Candle   | Gold                | Mask          | Entirely white eyes      |
|  02  | Sect        | Death          | Fish     | Dove                | Scarf         | Skin the texture of wood |
|  03  | Cult        | Light          | Sun      | Hunger              | Head covering | Teeth like a shark       |
|  04  | Hermitage   | Magic          | Crescent | Honey               | Hood          | Metallic hair            |
|  05  | Covenant    | Battle         | Star     | Sweat               | High Collar   | Double-jointed knees     |
|  06  | Temple      | Trade          | Hammer   | Feast               | Robe          | Skin hot to touch        |
|  07  | Choir       | Travel         | Wave     | Tin                 | Belt          | Voice rumbles in chest   |
|  08  | Chapter     | Sky            | Wheel    | Hair                | Ring          | Lips are violet          |
|  09  | Order       | Water          | Wings    | Copper              | Pendant       | Symbol is raised on palm |
|  10  | Sanctuary   | Darkness       | Drop     | Art                 | Tattoo        | Smell like fire          |
|  11  | Inquisition | Chaos          | Leaf     | Blood               | Sash          | Ears are huge            |
|  12  | Flock       | Community      | Flame    | Phoenix             | Stole         | Cast no shadow           |

{{% example "Religious Organization" %}}
We first roll 1D12 6 times to determine which details will define this organization:

- **Type:** 11, Inquisition
- **Primary Domain:** 11, Chaos
- **Symbol:** 2, Fish
- **Preferred Sacrifice:** 10, Art
- **Cleric Attire:** 6, Robe
- **Distinctive Feature:** 11, Ears are huge

### The Troutslappers of Her Aquatic Majesty

The adherents of Nianzhen, the Mermaid Queen, are a terrifying force in the canals and spires of Pentola.
These devout souls carry their holy symbol, a two foot long solid bronze trout, as a weapon and a warning.
They stalk the city in deep red robes and seek out those who would break Nianzhen's commandments - which change daily depending on the inquisitor.

They practice their faith by routinely drowning and reviving each other in an attempt to get closer to Her Aquatic Majesty and understand her mysteries more completely.
Their only unwavering belief is that art, especially statues, are an affront to Her - after all, water is formless.

Even when out of their vestments these pious interlocutors are visible by their massive ears - all the better to hear blasphemy on the lips of citizens...
{{% /example %}}

### Guild

Roll once on the [Vocations](../vocations-and-skills) table, then roll 1D12 6 times:

| 1D12 |    Type     | Reputation  |   Symbol  |    Value     |  Color   |    Distinctive Feature     |
|:----:|:-----------:|:-----------:|:---------:|:------------:|:--------:|:--------------------------:|
|  01  | Association | Trustworthy | Scales    | Profit       | Red      | Eyes glow in darkness      |
|  02  | Society     | Ambitious   | Crow      | Reputation   | Blue     | Skin is textured like silk |
|  03  | Shreni      | Violent     | Honeycomb | Favors       | Yellow   | Bulging eyes               |
|  04  | League      | Aggressive  | Fruit     | Customers    | Green    | Skull shaped like cone     |
|  05  | Company     | Dependable  | Knife     | Competition  | Purple   | Irises are square          |
|  06  | Esnaf       | Duplicitous | Ladder    | Transparency | Brown    | Very long fingers          |
|  07  | House       | Impuslive   | Moon      | Improvement  | White    | Hairy feet                 |
|  08  | Order       | Talkative   | Anchor    | Discipline   | Gray     | Double-jointed elbows      |
|  09  | Body        | Articulate  | Bone      | Leadership   | Black    | Missing finger             |
|  10  | Conjuration | Industrious | Razor     | Service      | Orange   | Sweats capsacin            |
|  11  | Union       | Humble      | Bell      | Quality      | Pink     | Skin gritty like sand      |
|  12  | Cooperative | Sardonic    | Tool      | Safety       | Numerous | Skin clinks like metal     |

### Spire
| 1D12 |    Shape   |  Symbol  |  Reputation  |    Value   |     Focus     |   Distinctive Feature    |
|:----:|:----------:|:--------:|:------------:|:----------:|:-------------:|:------------------------:|
|  01  | Spiraling  | Griffin  | Brave        | Money      | Transcendance | Metallic veins           |
|  02  | Sheer      | Squid    | Adventurous  | Power      | Automata      | Silver tears             |
|  03  | Oval       | Eagle    | Vengeful     | Secrets    | Magic         | Blood is flammable       |
|  04  | Finger     | Scorpion | Creative     | Artifacts  | Supplication  | Nose like snake          |
|  05  | Branching  | Bat      | Arrogant     | Art        | Poetry        | Full mane                |
|  06  | Diamond    | Elephant | Devout       | Books      | Architecture  | Entirely black eyes      |
|  07  | Octagaonal | Crab     | Charitable   | Loyalty    | Religion      | Abnormally tall          |
|  08  | Star       | Moon     | Jealous      | Neutrality | Control       | Prehensile hair          |
|  09  | Monolithic | Serpent  | Manipulative | Law        | Military      | Breath frosts the air    |
|  10  | Fractal    | Feather  | Prim         | Fame       | Pleasure      | Skin looks like opal     |
|  11  | Spear      | Dagger   | Rigid        | Glory      | Recognition   | Body covered in soft fur |
|  12  | Polygonal  | Lens     | Tireless     | Peace      | Wealth        | Ears are just holes      |