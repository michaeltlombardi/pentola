---
title: "Determining Characteristics"
weight: 100
---

Characters in Pentola have three characteristics: Body (BOD), Intellect (INT), and Power (POW).

Body represents a character's physical prowess, including their strength, agility, toughness, hand-eye coordination, etc.
Intellect represents a character's mental prowess, including raw intelligence, cunning, intuition, etc.
Power represent's a character's force of will and (if applicable) metaphysical aptitude (raw magic, psychic aptitude, etc).

You determine your starting characteristics by rolling three six-sided dice (3d6) for each characteristic in order.
You roll first for body, then for intellect, and finally for power.

{{% example "Summing 3d6" %}}
When determining a characteristic you roll 3d6.
Assuming you rolled a `6`, a `1`, and a `3`, the sum of these results would be 10.
{{% /example %}}

A 10 in a characteristic is the average for a human - higher scores are above average - and a 20 is the absolute upper bound of a human not affected by special abilities or conditions (magic, cybertech, etc).

Characteristics give a base goal for tests as 3x the characteristic.

{{% example "Determining Characteristics for Karacter" %}}
To determine Karacter's characteristics, we roll 3d6 each for BOD, INT, and POW:

+ BOD: 13
+ INT: 10
+ POW: 6

At the beginning of play Karacter is stronger, tougher, and more agile than most people, has an average mind, and has significantly less personal power than most people.

Because the base goal for tests is 3x a characteristic, Karacter's stat block ends up looking like this so far:

+ BOD: 13 (39%)
+ INT: 10 (30%)
+ POW: 6 (18%)
{{% /example %}}

Declare _what_ you're trying to accomplish (your intent) **and** _how_ you intend to accomplish it (your approach).
The referee will determine if your action requires a test and which characteristic is most appropriate.

Then you'll try to roll under the appropriate goal.
Your characteristics determine your base chance of success at any given test, but you'll _usually_ be able to add a bonus from a relevant skill, vocation, or ability.