+++
title = "Vocations and Skills"
description = "Determining Vocation and Skills"
weight = 110
+++

## Vocations
Characters each have a vocation - a calling or an occupation to which a character is inexorably drawn and which helps to define them.

Whenever your character is faced with a test which can be reasonably argued to be relevant to their vocation you may add your vocation bonus to the test's goal.

Every character begins play with a single vocation at 25% and can optionally acquire a second vocation later by spending five IP.
No character can have more than two vocations at a single time.

If you want to trade one of your vocations for another, you can do so freely, though the bonus resets to 25%.

You can write your own vocation, roll to determine your vocation based on the table below, or choose a vocation that you think fits your character.

| d100 Result | Vocation       | d100 Result | Vocation       |
|:-----------:|:--------------:|:-----------:|:--------------:|
|     01      | Acrobat        |     51      | Hatter         |
|     02      | Actor          |     52      | Historian      |
|     03      | Alchemist      |     53      | Illuminator    |
|     04      | Apothecary     |     54      | Inventor       |
|     05      | Archaeologist  |     55      | Investigator   |
|     06      | Archer         |     56      | Jester         |
|     07      | Architect      |     57      | Jeweler        |
|     08      | Aristocrat     |     58      | Journalist     |
|     09      | Artist         |     59      | Judge          |
|     10      | Assassin       |     60      | Lawyer         |
|     11      | Automator      |     61      | Librarian      |
|     12      | Barber         |     62      | Linguist       |
|     13      | Barrister      |     63      | Locksmith      |
|     14      | Bodyguard      |     64      | Mason          |
|     15      | Bookbinder     |     65      | Mercenary      |
|     16      | Bounty Hunter  |     66      | Merchant       |
|     17      | Brewer         |     67      | Mortician      |
|     18      | Butcher        |     68      | Notary         |
|     19      | Canal Purifier |     69      | Prostitute     |
|     20      | Carpenter      |     70      | Politician     |
|     21      | Cartographer   |     71      | Preacher       |
|     22      | Cleric         |     72      | Priest         |
|     23      | Clock Maker    |     73      | Printer        |
|     24      | Cobbler        |     74      | Privateer      |
|     25      | Cook           |     75      | Procurer       |
|     26      | Cooper         |     76      | Performer      |
|     27      | Courtesan      |     77      | Reeve          |
|     28      | Cutler         |     78      | Researcher     |
|     29      | Cutpurse       |     79      | Retainer       |
|     30      | Dancer         |     80      | Saddler        |
|     31      | Dentist        |     81      | Sailor         |
|     32      | Diabolist      |     82      | Scribe         |
|     33      | Diplomat       |     83      | Sculptor       |
|     34      | Drake Jockey   |     84      | Smith          |
|     35      | Duellist       |     85      | Soldier        |
|     36      | Enchanter      |     86      | Spelunker      |
|     37      | Exorcist       |     87      | Spirit Binder  |
|     38      | Explorer       |     88      | Spy            |
|     39      | Falconer       |     99      | Summoner       |
|     40      | Fencer         |     90      | Surgeon        |
|     41      | Firefighter    |     91      | Tailor         |
|     42      | Fisher         |     92      | Teacher        |
|     43      | Florist        |     93      | Thief          |
|     44      | Fortuneteller  |     94      | Torturer       |
|     45      | Gardener       |     95      | Toymaker       |
|     46      | Gladiator      |     96      | Tutor          |
|     47      | Glider         |     97      | Vatter         |
|     48      | Gondolier      |     98      | Weaver         |
|     49      | Grocer         |     99      | Wizard         |
|     50      | Guard          |     00      | Writer         |

## Choosing Skills
s
Skills are things a character specializes in doing.
At the beginning of play, a character has 250 percentage points to spend on skills.
In Pentola there is no defined skill list, but here are a few examples for inspiration:

> Dodge, Archery, Fencing, Culture (Gascon), Alchemy, Athletics, Sculpting, Driving, Influence, Mechanisms, Streetwise, Trade, Literacy (Pentolan), Occult Lore, Medicine, Wrestling, Painting, Swimming, Thaumaturgy, Drakeback Riding, Herbalism, Mathematics, Goldsmithing, Scultping, Researching, Stealth, Haggling,etc.

Note that some of the skills overlap or seem a bit random - pick skills that you want your character to specialize in, then use those specializations to solve problems.
The list is freeform so you can capture _exactly_ what makes your character yours.

The highest relevant skill or vocation bonus (if any) is always added to the character's goal for a test.

### Archetype Skills

If you are using an optional archetype, as detailed previously, some of your skills are predetermined for you.

Instead of having 250 points to spend, you gain the skills listed for your archetype and have 50 points left to distribute.

|    Archetype    | Skills (Bonus %) |
|:---------------:|:-----------------|
| Sword and Board | Sword and Buckler (35), Athletics (35), Influnce (30), Streetwise (20), Haggling (20), Swimming (20), Military Lore (20), Heraldry (20), and Dodge (20).
| Archer          | Marksman (35), Athletics (35), Perception (30), Streetwise (20), Haggling (20), Fletching (20), Hunting (20), and Stealth (20).
| Thief           | Stealth (35), Acrobatics (35), Streetwise (30), Pickpocket (20), Lockpicking (20), Perception (20), Reconnaissance (20), and Marksman (20).
| Alchemist       | Alchemy (35), Research (35), Trade (30), Literacy [Pentolan] (20), Crafting (20), Nature Lore (20), Medicine (20), and Appraisal (20).
| Priest          | Influence (35), Religious Lore (35), Diabolism (30), Crafting (20), Investigation (20), Literacy (20), Occult Lore (20), and Alchemy (20).
| Mage            | Crafting (35), Literacy [Pentolan] (35), Researching (30), Alchemy (20), Occult Lore (20), Diabolism (20), Dodge (20), and Appraisal (20).
| Knight          | Sword and Buckler (35), Drakerider (35), Marksmanship (30), Influence (20), Trade (20), Literacy [Pentolan] (20), Heraldry (20), and Swimming (20).
| Con Artist      | Influence (35), Streetwise (35), Deception (30), Legal Lore (20), Appraisal (20), Mechanisms (20), Haggling (20), and Brawling (20).
| Artist          | Crafting (35), Sculpting (35), Trade (30), Research (20), Mechanisms (20), Alchemy (20), Literacy [Pentolan] (20), and Drawing (20). May trade Sculpting for any other type of art.
| Leader          | Influence (35), Diplomacy (35), Politics (30), Legal Lore (20), Literacy [Pentolan] (20), Investigation (20), Sword and Buckler (20), and Dodge (20).