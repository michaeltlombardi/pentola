---
title: "Reputations"
weight: 130
---

Reputations are statements or adjectives about characters that other people may believe.
For example, a character might have a reputation for being unlucky, or frightful, or earnest, or for being a priest-killer, or for being timid, or any number of other possibilities.

Reputations function as a special type of skill and have a percentage bonus.
Whenever interacting with someone who might know the character’s reputation, and in the first situation where that reputation might matter, make a POW test, adding the specific reputation bonus.
If successful, the person the character is interacting with believes in the character’s reputation.
If unsuccessful, the person does not believe in the reputation.
In either case, do not test for that reputation again with this person unless the reputation bonus changes.

In general, reputations can make social tests either one step easier or more difficult, depending on the reputation and the context.
In some cases, a reputation may even make a test unnecessary.

Characters begin play with two reputations, either from the table below or of your choice, both starting at 25%.

Every time a character performs an action where the action can be observed or recorded _and_ the action could either reinforce or disabuse observers of the reputation, place a mark to the left (if the action could reinforce the reputation) or the right (if the action could disabuse observers of the reputation).
At the completion of a quest or when beginning an interlude - whenever there’s a sensible break in the quest to reflect and take time to recover - players should roll 1D100 for each mark beside their reputations.

For each mark to the left of the reputation, roll 1D100.
If the result of the roll is higher than the reputation, increase the reputation by 3.

For each mark to the right of the reputation, roll 1D100.
If the result of the roll is lower than the reputation, decrease the reputation by 3.

Characters can also spend IP to improve or reduce a reputation, improving or reducing it by 5% for each IP spent.
Characters can have any number of different reputations.

| d100 Result | Reputation     | d100 Result | Reputation     |
|:-----------:|:--------------:|:-----------:|:--------------:|
|     01      | Power-hungry   |     51      | Eloquent       |
|     02      | Disobedient    |     52      | Silly          |
|     03      | Glamorous      |     53      | Childish       |
|     04      | Ascetic        |     54      | Dishonest      |
|     05      | Invulnerable   |     55      | High-spirited  |
|     06      | Irreverant     |     56      | Crafty         |
|     07      | Impressive     |     57      | Principled     |
|     08      | Studious       |     58      | Stiff          |
|     09      | Faithful       |     59      | Leaderly       |
|     10      | Gentle         |     60      | Ignorant       |
|     11      | Moralistic     |     61      | Erratic        |
|     12      | Argumentative  |     62      | Peaceful       |
|     13      | Ridiculous     |     63      | Familial       |
|     14      | Messy          |     64      | Attractive     |
|     15      | Neat           |     65      | Inhibited      |
|     16      | Teacherly      |     66      | Upright        |
|     17      | Blunt          |     67      | Boisterous     |
|     18      | Winning        |     68      | Passionate     |
|     19      | Narrow-minded  |     69      | Perverse       |
|     20      | Dry            |     70      | Friendly       |
|     21      | Retiring       |     71      | Capable        |
|     22      | Planful        |     72      | Systematic     |
|     23      | Well-read      |     73      | Breezy         |
|     24      | Criminal       |     74      | Tolerant       |
|     25      | Insensitive    |     75      | Deep           |
|     26      | Bizarre        |     76      | Perceptive     |
|     27      | Elegant        |     77      | Imprudent      |
|     28      | Quiet          |     78      | Cynical        |
|     29      | Steady         |     79      | Unlovable      |
|     30      | Reactive       |     80      | Formal         |
|     31      | Unlucky        |     81      | Troublesome    |
|     32      | Nihilistic     |     82      | Unreliable     |
|     33      | Iconoclastic   |     83      | Stoic          |
|     34      | Idiosyncratic  |     84      | Polished       |
|     35      | Passive        |     85      | Confused       |
|     36      | Forthright     |     86      | Discouraging   |
|     37      | Protective     |     87      | Distractible   |
|     38      | Undemanding    |     88      | Purposeful     |
|     39      | Unchanging     |     99      | Lyrical        |
|     40      | Subjective     |     90      | Narrow         |
|     41      | Unsentimental  |     91      | Curious        |
|     42      | Conceited      |     92      | Self-defacing  |
|     43      | Frivolous      |     93      | Huried         |
|     44      | Disputatious   |     94      | Colorful       |
|     45      | Greedy         |     95      | Soft           |
|     46      | Objective      |     96      | Critical       |
|     47      | Good-natured   |     97      | Courteous      |
|     48      | Enthusiastic   |     98      | Puritanical    |
|     49      | Respectful     |     99      | Busy           |
|     50      | Casual         |     00      | Lazy           |