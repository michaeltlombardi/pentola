---
title: "Character Creation"
weight: 110
---

Characters are creating by following these steps:

1. Determining characteristics
2. Calculating attributes
3. Choosing vocation and skills
4. Choosing relationships
5. Choosing reputations
6. Determining wealth and buying equipment
7. Choosing knacks
8. Setting ambitions