---
title: "Alternate Knacks"
weight: 161
---

{{% alert theme="warning" %}} 
**WARNING:** These are alternate rules for knacks and are not _yet_ tested.
Characters should use _either_ the traditional knacks _or_ alternate knacks, but not both.
{{% /alert %}}

Knacks are special abilities characters can possess, tied to _who they are_ and _what they repeatedly do_ - a character's knacks are a statement about a character's _gestalt_, the qualities which are more than the sum of a character's individual parts.
Knacks derive from personality traits, behaviors, vocations, and core beliefs.

Characters begin play with three randomly determined knacks at magnitude 3.
To determine a new knack, roll a d4 one the formula table and then a d30 four times to determine the quirk, form, effect, and element.
If the combination doesn't make sense, roll again or pick an appropriate option from the list.

Some knacks may be predetermined and provided by the referee as resulting from a particular organization, skill, or vocation.

Knacks gained after character creation always start at magnitude 1.
The maximum magnitude of any knack is limited by a character's POW.
For example, a character with a POW of 12 could not have any knacks whose magnitude was 13 or higher.
Note that this limits the maximum magnitude of any given knack, not the total magnitude of knacks a character can acquire.

| Roll d8 |                               Description                                |
|:-------:|:------------------------------------------------------------------------:|
|    1    | (Physical Quirk) (Physical Form) of (Ethereal Effect) (Ethereal Element) |
|    2    | (Physical Quirk) (Physical Form) of (Ethereal Effect) (Physical Element) |
|    3    | (Physical Quirk) (Physical Form) of (Physical Effect) (Physical Element) |
|    4    | (Physical Quitk) (Physical Form) of (Physical Effect) (Ethereal Element) |
|    5    | (Ethereal Quirk) (Ethereal Form) of (Ethereal Effect) (Ethereal Element) |
|    6    | (Ethereal Quirk) (Ethereal Form) of (Ethereal Effect) (Physical Element) |
|    7    | (Ethereal Quirk) (Ethereal Form) of (Physical Effect) (Physical Element) |
|    8    | (Ethereal Quitk) (Ethereal Form) of (Physical Effect) (Ethereal Element) |

| Roll d30 | Physical Quirk | Physical Form | Ethereal Quirk | Ethereal Form | Physical Effect | Ethereal Effect | Physical Element | Ethereal Element |
|:--------:|:--------------:|:-------------:|:--------------:|:-------------:|:---------------:|:---------------:|:----------------:|:----------------:|
|    01    | Atrophied      | Ankle         | Soft           | Aura          | Animating       | Avenging        | Acid             | Ash              |
|    02    | Bevelled       | Arm           | Ringing        | Bile          | Attracting      | Banishing       | Amber            | Chaos            |
|    03    | Bloody         | Back          | Metallic       | Blood         | Binding         | Bewildering     | Bark             | Distortion       |
|    04    | Burned         | Body          | Congealed      | Blush         | Blossoming      | Blinding        | Blood            | Dream            |
|    05    | Dark           | Brow          | Bitter         | Breath        | Consuming       | Charming        | Bone             | Dust             |
|    06    | Extra          | Cheek         | Sweet          | Hearing       | Creeping        | Compelling      | Brine            | Fire             |
|    07    | Gnarled        | Chest         | Lingering      | Heart         | Crushing        | Concealing      | Clay             | Fog              |
|    08    | Hairy          | Ear           | Sharp          | Laugh         | Diminishing     | Deafening       | Crow             | Harmony          |
|    09    | Jeweled        | Elbow         | Dull           | Liver         | Duplicating     | Deceiving       | Crystal          | Heat             |
|    10    | Large          | Eye           | Full           | Marrow        | Enveloping      | Disguising      | Ember            | Light            |
|    11    | Long           | Finger        | Sickly         | Mind          | Expanding       | Dispelling      | Fungus           | Lightning        |
|    12    | Muscular       | Foot          | Harsh          | Mood          | Fusing          | Emboldening     | Glass            | Memory           |
|    13    | Pale           | Hair          | Burning        | Movement      | Hastening       | Energizing      | Honey            | Mutation         |
|    14    | Polished       | Hand          | Icy            | Phlegm        | Hindering       | Enlightening    | Ice              | Negation         |
|    15    | Rough          | Jaw           | Smooth         | Presence      | Illuminating    | Enraging        | Insect           | Plague           |
|    16    | Sandy          | Knee          | Staggered      | Reflection    | Imprisoning     | Excruciating    | Wood             | Probability      |
|    17    | Scaled         | Lips          | Memorable      | Scent         | Opening         | Intoxicating    | Lava             | Rain             |
|    18    | Scarred        | Nail          | Bland          | Sense         | Petrifying      | Maddening       | Moss             | Rot              |
|    19    | Short          | Neck          | Pungent        | Shadow        | Phasing         | Mesmerizing     | Obsidian         | Shadow           |
|    20    | Small          | Nose          | Clear          | Sight         | Piercing        | Mindreading     | Oil              | Smoke            |
|    21    | Smooth         | Nostril       | Repuslive      | Sound         | Pursuing        | Nullifying      | Poison           | Snow             |
|    22    | Spiked         | Palm          | Hastened       | Echo          | Reflecting      | Paralyzing      | Rat              | Soul             |
|    23    | Split          | Shoulder      | Hardened       | Sweat         | Rending         | Revealing       | Sand             | Star             |
|    24    | Sticky         | Skin          | Resonant       | Taste         | Repelling       | Revolting       | Serpent          | Stasis           |
|    25    | Swollen        | Skull         | Stinging       | Tears         | Screaming       | Silencing       | Slime            | Steam            |
|    26    | Symetrical     | Stomache      | Shimmering     | Timbre        | Sealing         | Soothing        | Stone            | Thunder          |
|    27    | Thick          | Teeth         | Fearsome       | Touch         | Shapeshifting   | Terrifying      | Thorn            | Time             |
|    28    | Thin           | Toe           | Animalistic    | Vitality      | Shielding       | Warding         | Vine             | Warp             |
|    29    | Twisted        | Tongue        | Ancient        | Voice         | Spawning        | Wearying        | Water            | Whisper          |
|    30    | Wiry           | Bones         | Weak           | Yawn          | Transmuting     | Withering       | Wood             | Wind             |


Once you've determined the description of the knack the referee describes the knack's general effects based on its name and works with you to explain how it ties into your character's gestalt.
If a knack is related to a vocation, skill, or relationship its magnitude can never exceed the 10s place of the relevant bonus (in addition to the limitation on all knacks imposed by your Power score).

If a character exercises a knack against an unwilling target an opposed test is required.
When an opposed test is required, the character exercising their knack always rolls against POW, adding the highest relevant vocation, skill, or relationship bonus if related.
The target rolls against BOD (if the knack is something that can be dodged) or POW (if the knack is something that can be resisted by force of will) and adds their highest relevant bonus as normal.

Unless otherwise specified, a knack's ongoing effects last up to 10 minutes per magnitude and has a range of nearby.
If a knack deals damage instantly it deals 1d6 per magnitude if it is reduced by armor or 2 per magnitude if it is not.
If a knack increases/decreases damage dealt or armor for a duration it does so for 1 point per magnitude.
If a knack imposes a penalty or grants a bonus actively it does so at 10% per magnitude.
If a knack imposes a penalty or grants a bonus passively it does so at 5% per total magnitude.
A knack which grants an extra action or reaction is triggered once in its duration; after the extra actions or reactions are used the knack ends.
If a knack has no mechanical effects which improve with the magnitude, instead grant the character a 10% bonus per magnitude to their opposed POW test to exercise the knack against unwilling targets.

You may also suggest uses for the knack outside of the referee's listed effect.
If the knack fits the description and situation closely the referee should usually allow it.

{{% example "Creating a Knack" %}}
To create a knack we first roll a d8 and get a 4.
This indicates that the description of our knack will match the following description:

> `(Physical Quirk) (Physical Form) of (Physical Effect) (Ethereal Element)`.

We now roll a d30 four times to determine each piece: 14, 20, 26, 8.

This results in the knack "Polished Nose of Sealing Harmony"

The referee interprets this as Karacter's nose appearing to be polished to a smooth shine and, when exercised (and thereby empowered) causes those within range who can see them to have their violent and discordant thoughts and tendencies sealed away for the duration.

This is linked to Karacter's deeply held belief that problems can be solved by compromise and careful consideration.
For every additional point of magnitude this knack will grant a 10% bonus to Karacter's opposed POW test to make this knack affect targets.
{{% /example %}}

## Exercising a Knack
Exercising a knack is an action and takes something out of the character.
To exercise a knack a character spends an amount of power points equal to the magnitude at which they want to exercise the knack.
Characters have some control over how much of themselves they pour into the exercising of a knack and _do not_ have to use their knacks at the highest magnitude.

{{% example "Exercising a Knack" %}}
Karacter chooses to exercise their "Polished Nose of Sealing Harmony" knack in response to escalating tensions between two factions.
They have the knack at magnitude 5 but choose to exercise it as magnitude 3.

Karacter then makes an opposed test against everyone nearby, testing their POW with a +30% bonus from the knack's magnitude.
The effected targets also test POW and add the highest relevant bonus, if any.
{{% /example %}}

A character does not need to make any tests to exercise a knack because the ability is an extension of who they are (though an opposed test may need to be made to effect the target as normal).
It uses some metaphysical energy that they experience as a growing sense of hunger and ache in their bones, but does not otherwise tire them.

If a character has no power points to spend on exercising a knack they may instead make a Power test and spend HP equal to the magnitude of the knack they are attempting to exercise.
Note that the HP is lost _whether or not_ they succeed on the test as they burn some of their life force to power the attempt.
Failure indicates that they simply couldn't control it.

{{% example "Exercising a Knack at 0 PP" %}}
Karacter chooses to exercise their "Polished Scent of Sealing Harmony" knack in response to escalating tensions between two factions in a last ditch attempt to prevent violence from breaking out.
They have the knack at a magnitude 5 and choose to exercise it as such, but have no PP left.

Karacter taps into their life force, making a POW test and succeeding; their current HP decreases by 5.
They then make an opposed test against everyone nearby, testing their POW with a +50% bonus from the knack's magnitude.
The effected targets also test POW and add the highest relevant bonus, if any.

**Note:** If Karacter had failed their POW test to exercise the knack they would _still_ have had their current HP decrease by 5.
{{% /example %}}

## Improving Knacks

When spending improvement points you may spend 1IP to increase the magnitude of a knack by 1, provided you meet the prerequisites (if any).