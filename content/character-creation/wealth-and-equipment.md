---
title: "Wealth and Equipment"
weight: 140
---

In Pentola, characters are assumed to start with a default wealth of _average_.
You can, instead, opt to bein play with three additional improvement points per wealth level you forego.

| IP Cost |    Level   |        Social Class        | Usage Die |Equipment Examples |
|:-------:|:----------:|:--------------------------:|:---------:|:------------------|
|    1    | Destitute  | Beggars amd serfs          | **d4**    | A glass of wine
|    4    | Poor       | Free laborers              | **d6**    | Pound of salt pork
|    7    | Average    | Tradesfolk                 | **d8**    | Tool
|    11   | Prosperous | Minor merchants, officials | **d10**   | Fine Wine bottle
|    15   | Wealthy    | Merchants, minor nobility  | **d12**   | War horse
|    20   | Rich       | Spirons, Guildheads        | **d20**   | Building

Whenever you make a sizable purchase, including buying starting equipment, roll your usage die.
On a roll of a 1, your usage die decreases in size as normal.
Once your usage die has been fully expended, your wealth drops by one rank and you gain a new usage die appropriate to your new wealth.

Whenever you make a sizable sale, roll your usage die.
If the result of the roll is the maximum for that die your usage die increases one step in size, up to the maximum for that wealth level.

Later, when improving your character, you can spend a number of improvement points equal to the difference between your current wealth and the desired rank.

{{% example "Improving Wealth" %}}
Karacter started play with Average wealth and wants to increase it to Well-Off.

The difference in IP costs is 11 - 7, so Karacter would pay 4 IP to increase their wealth.

If Karacter wanted to instead increase their wealth from Average to Wealthy, they would pay 8 IP.
{{% /example %}}

### Encumbrance

A character can carry a number of items equal to their BOD with no issues.
Carrying over this amount means they are encumbered and all tests are one step harder - they can also only ever move to somewhere nearby in a moment.
They simply cannot carry more than double their BOD.

### Armor

Armor provides protection (AP) by reducing all incoming damage.
When taking damage from an attack reduce the incoming damage by the AP.
However, heavier armors impose penalties, making some tests one step harder.

Armors also have a usage die which must be rolled after every combat in which damage was taken that the armor did not completely absorb.

| Type   | AP | Usage Die | Affected Tests                  |
|:------:|:--:|:---------:|:-------------------------------:|
| Light  | 3  |     D6    | None                            |
| Medium | 5  |     D8    | Athletics and fatigue           |
| Heavy  | 7  |     D10   | Athletics, fatigue, and stealth |

### Weapons and Shields

Weapons and shields have sizes - when a character uses their reaction to parry an attack, they can negate all of the damage on a successful parry if their weapon or shield is the same size; if the parrying weapon/shield is one size smaller it blocks half the damage; if the parrying weapon/shield is more than one size smaller it blocks none of the damage.

Weapons and shields have a damage die which is rolled to determine how much HP damage is inflicted on the target.
Remember that AP and parrying can reduce incoming damage to zero.

In addition to damage, weapons and shields also have a usage die.
For weapons, the usage die is rolled after every combat where the weapon fails to damage an enemy because all of the damage was negated by AP.
For shields, the usage die is rolled after every combat where the shield is successfully used to parry an attack against a weapon.

| Weapon           | Usage Die | Damage |
|:-----------------|:---------:|:------:|
| Small 1H Weapon  |    D8     |   1D6  |
| Medium 1H Weapon |    D8     |   1D8  |
| Large 2H Weapon  |    D8     |   2D6  |
| Small Shield     |    D6     |   D4   |
| Medium Shield    |    D8     |   D6   |
| Large Shield     |   D10     |   D8   |
| Huge Shield      |   D12     |   D10  |

### Miscellaneous Equipment Table

| Item                     | Usage Die |               Notes               |
|:-------------------------|:---------:|:---------------------------------:|
| Backpack                 |     -     |           Carry +2 extra          |
| Flask of Oil             |    D6     |                 -                 |
| Tools                    |     -     | Related tests are one step easier |
| Lantern                  |     -     |                 -                 |
| Handheld Mirror          |     -     |                 -                 |
| Preserved Rations        |    D8     |                 -                 |
| Fresh Rations            |    D4     |                 -                 |
| 50' Rope                 |     -     |                 -                 |
| Small Sack               |     -     |                 -                 |
| Large Sack               |     -     |                 -                 |
| Flint & Steel            |     -     |                 -                 |
| Torches (6)              |    D6     |     Each Torch has a usage die    |
| Wineskin                 |    D6     |                 -                 |
| Assorted Common Herbs    |    D8     |                 -                 |
| 10' Pole                 |     -     |                 -                 |
| Quiver of Arrows / Bolts |    D10    |                 -                 |

{{% example "Determining Wealth and Buying Equipment for Karacter" %}}
We decide to have Karacter forego one level of wealth and begin play as Poor to gain an addition three improvement points immediately.

This means picking up starting equipment is going to be a bit dicey.

Karacter needs heavy armor, rations, a wineskin, a longbow, arrows, a lantern, oil, and herbs - but with a Wealth of Poor, Karacter may have their usage die reduced by one or more steps - maybe even becoming destitute!

We roll for each item:

+ Heavy Armor (4 - usage die does not reduce)
+ Rations (6 - no change)
+ Wineskin (1 - usage die is reduced to 1D4!)

At this point, Karacter elects not to continue purchasing starting equipment, picking the negligible cost of a quarterstaff instead of the initially-preferred longbow etc.
This alters our plans for Karacter a bit, but nothing we can't work around.

{{% /example %}}
