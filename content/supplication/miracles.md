---
title: Miracles
weight: 130
---

In order to acquire any miracles, the character must be a supplicant, champion, or principal within an organization.
Lay members are not entrusted or empowered with the organization's miracles.

## Acquiring a Miracle

The character must either go to an organizational headquarters or commune with their grantor and request to be empowered with a miracle.
They must invest 2 IP per point of magnitude for the miracle they wish to acquire.
These IP are not regained by leaving the organization or deciding not to use the miracle any longer.

A character can incrementally improve the magnitude of their miracles by spending additional IP during another visit to the headquarters or while communing with their grantor.

## Performing a Miracle

A character must be able to gesture with his hands and be able to chant in order to perform a miracle.
Whenever a miracle is performed, there will always be a sight and sound that nearby creatures can detect, be it a flash of light, a crack of thunder or a shimmering in the air.
The exact effects are up to the referee and player to decide but will automatically be detected by any creatures who are nearby.

Performing a miracle is automatically successful.
No dice need be rolled, no chance of a fumble or critical either.
Miracles do not cost any PP and always take only a single action to perform.

A character can dismiss any miracle they have performed as a single action.
Ceasing to perform a concentration miracle is immediate and does not require an action.

Miracles do not stack, only the most powerful effect from overlapping bonuses or penalties applies.

When in a direct contest with either knacks or dweomers, miracles are considered to have double their normal magnitude.

### Splitting Magnitude

A character can 'split' a miracle's magnitude into multiple miracles.

For instance, if the character knows the Absorption miracle at magnitude 3, they may choose to perform it as:

- a single magnitude 3 miracle
- three magnitude 1 miracles
- one magnitude 1 and one magnitude 2 miracle.

The split miracles are treated as separate instances and are performed separately.

### Perform Once Only

Each Miracle may be performed only once, after which the character must make contact with a Grantor of the organization and petition them for more power.

This is done by making a hard relationship test.
Failure causes the supplicant to regain only half their spent miracles and requires them to perform a quest for the grantor before the other half are returned.

The character need not spend any improvement points to regain their miracles.